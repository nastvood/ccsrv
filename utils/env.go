package srv

import (
	"bufio"
	"log"
	"os"
	"strings"
)

var (
	fEnvs       map[string]string
	fEnvsLoaded bool
)

const fEnvFileName = ".env"

func removeQuote(s string) string {
	len := len(s)

	if len < 2 {
		return s
	}

	if (s[0] == '\'' && s[len-1] == '\'') ||
		(s[0] == '"' && s[len-1] == '"') {
		return s[1 : len-1]
	}

	return s
}

func parsefEnvs() {
	if fEnvsLoaded {
		return
	}

	file, err := os.Open(fEnvFileName)

	if err != nil {
		log.Printf("Failed load %s file: %s \n", fEnvFileName, err.Error())
		return
	}

	defer file.Close()

	scanner := bufio.NewScanner(bufio.NewReader(file))
	scanner.Split(bufio.ScanLines)

	kvTmp := make(map[string]string)

	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "//" || line == "" {
			continue
		}

		parts := strings.SplitN(line, "=", 2)

		if len(parts) < 2 {
			kvTmp[parts[0]] = ""

			continue
		}

		kvTmp[parts[0]] = removeQuote(parts[1])

	}

	fEnvs = make(map[string]string)

	for k, v := range kvTmp {
		fEnvs[k] = v
	}

	fEnvsLoaded = true
}

func Getenv(key string) string {
	if v, ok := os.LookupEnv(key); ok {
		return v
	}

	if !fEnvsLoaded {
		parsefEnvs()
	}

	return fEnvs[key]
}

func LookupEnv(key string) (string, bool) {
	if v, ok := os.LookupEnv(key); ok {
		return v, ok
	}

	if !fEnvsLoaded {
		parsefEnvs()
	}

	v, ok := fEnvs[key]

	return v, ok
}
