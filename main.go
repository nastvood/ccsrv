package main

import (
  srv "ccsrv/srv"
)

func main() {
  server := srv.NewServer(srv.InitConfig())

  server.Run()
}
