FROM golang:1.16.4-buster AS build

WORKDIR $GOPATH/src

COPY . .
RUN go build -o bin/ccsrv ccsrv

EXPOSE 8081 8082 8083

CMD ["bin/ccsrv", "-h", "0.0.0.0"]

