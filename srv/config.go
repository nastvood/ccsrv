package srv

import (
	utils "ccsrv/utils"
	"flag"
	"log"
	"strconv"
	"strings"
)

const DEFAULT_HOST = "localhost"
const DEFAULT_PORT = 8081
const DEFAULT_IS_MASTER = false
const DEFAULT_RPS = 5
const DEFAULT_SCHEMA = "http"

const ALIVED_CHECKER_TIMEOUT = 5 //seconds
const ALIVED_CONN_TIMEOUT = 2    //seconds

type ConfigWorker struct {
	Address string
	Rps     uint16
}

type Config struct {
	Host     string
	Port     uint16
	IsMaster bool
	Rps      uint16
	Workers  []ConfigWorker
}

func parseWorkers(s string) []ConfigWorker {
	parts := strings.Split(strings.TrimSpace(s), ",")

	if s == "" {
		return []ConfigWorker{}
	}

	res := make([]ConfigWorker, len(parts))

	for i, w := range parts {
		addressRps := strings.Split(strings.TrimSpace(w), " ")
		if len(addressRps) > 1 {
			rps, err := strconv.ParseUint(addressRps[1], 10, 16)

			if err != nil {
				panic("Err parse workers")
			}

			res[i] = ConfigWorker{
				Address: addressRps[0],
				Rps:     uint16(rps),
			}
		}
	}

	return res
}

func isFlagPassed(name string) bool {
	found := false

	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})

	return found
}

func InitConfig() Config {
	fHost := flag.String("h", DEFAULT_HOST, "host")
	fPort := flag.Uint("p", DEFAULT_PORT, "port")
	fWorkers := flag.String("w", "", "list of workers with their rps (e. g. localhost:8082 5,localhost:8083 10)")
	fRps := flag.Uint("r", DEFAULT_RPS, "max request per second")
	fIsMaster := flag.Bool("master", DEFAULT_IS_MASTER, "is master, default value is worker")

	flag.Parse()

	host := *fHost
	if !isFlagPassed("h") {
		if v, ok := utils.LookupEnv("HOST"); ok {
			host = v
		}
	}

	port := uint16(*fPort)
	if !isFlagPassed("p") {
		if v, ok := utils.LookupEnv("PORT"); ok {
			tmpPort, err := strconv.ParseUint(v, 10, 16)
			if err == nil {
				port = uint16(tmpPort)
			}
		}
	}

	isMaster := *fIsMaster
	if !isFlagPassed("master") {
		if _, ok := utils.LookupEnv("MASTER"); ok {
			isMaster = true
		}
	}

	rps := uint16(*fRps)
	if !isFlagPassed("r") {
		if v, ok := utils.LookupEnv("RPS"); ok {
			tmpRps, err := strconv.ParseUint(v, 10, 16)
			if err == nil {
				rps = uint16(tmpRps)
			}
		}
	}

	workers := *fWorkers
	if !isFlagPassed("w") {
		if v, ok := utils.LookupEnv("WORKERS"); ok {
			workers = v
		}
	}

	config := Config{
		Host:     host,
		Port:     port,
		IsMaster: isMaster,
		Rps:      rps,
		Workers:  parseWorkers(workers),
	}

	log.Printf("Init config %+v\n", config)

	return config
}
