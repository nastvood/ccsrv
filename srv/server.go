package srv

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"reflect"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

const CONTEXT_BODY_KEY = 1

type Worker struct {
	alived   bool
	address  string
	proxy    *httputil.ReverseProxy
	reqCount uint32
	getCount uint32
	rps      uint16
}

type Server struct {
	config       Config
	workers      []Worker
	muReqCount   sync.Mutex
	reqCount     uint32
	getCount     uint32
	muNextWorker sync.Mutex
}

func NewServer(config Config) Server {
	workers := make([]Worker, len(config.Workers))

	for i, w := range config.Workers {
		target, errTarget := url.Parse(DEFAULT_SCHEMA + "://" + w.Address)
		if errTarget != nil {
			panic(errTarget)
		}

		proxy := httputil.NewSingleHostReverseProxy(target)

		workers[i] = Worker{
			proxy:   proxy,
			rps:     w.Rps,
			alived:  true,
			address: w.Address,
		}
	}

	return Server{
		config:  config,
		workers: workers,
	}
}

func parseRequest(buf []byte) bool {
	var order Order

	if err := json.Unmarshal(buf, &order); err != nil {
		log.Printf("Err parse order: %s (%s)\n", err.Error(), string(buf))
		return false
	}

	log.Printf("Get order: %+v\n", order)

	return true
}

func (s Server) response(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")

	var buf []byte
	if req.Body != nil {
		buf, _ = ioutil.ReadAll(req.Body)
	}

	defer req.Body.Close()

	if parseRequest(buf) {
		address := s.config.Host + ":" + strconv.Itoa(int(s.config.Port))
		fmt.Fprintf(*w, "{\"server\": \"%s\"}", address)

		return
	}

	http.Error(*w, http.StatusText(http.StatusUnprocessableEntity), http.StatusUnprocessableEntity)
}

func (s *Server) getWorker() *Worker {
	var res *Worker

	s.muNextWorker.Lock()

	for i := 0; i < len(s.workers); i++ {
		w := &s.workers[i]

		//log.Println("getWorker", s.workers[i].reqCount, w.getCount)
		if atomic.LoadUint32(&w.reqCount) >= uint32(w.rps) || !w.alived || atomic.LoadUint32(&w.getCount) >= uint32(w.rps) {
			continue
		}

		atomic.AddUint32(&s.workers[i].reqCount, 1)
		atomic.AddUint32(&s.workers[i].getCount, 1)

		res = w

		break
	}

	s.muNextWorker.Unlock()

	return res
}

func (s *Server) handleWorker(w http.ResponseWriter, req *http.Request) {
	s.response(&w, req)
}

func (s *Server) sendToPeer(w http.ResponseWriter, req *http.Request) {
	peer := s.getWorker()

	if peer == nil {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)

		return
	}

	//FIXME: Костыль для net.OpError,
	var buf []byte
	if req.Body != nil {
		buf, _ = ioutil.ReadAll(req.Body)
	}
	req.Body = ioutil.NopCloser(bytes.NewBuffer(buf))

	ctx := context.WithValue(req.Context(), CONTEXT_BODY_KEY, buf)

	peer.proxy.ServeHTTP(w, req.WithContext(ctx))

	//peer.proxy.ServeHTTP(w, req)

	return
}

func (s *Server) handleMaster(w http.ResponseWriter, req *http.Request) {
	//FIXME проверить токенa

	/*9if (s.reqCount >= uint32(s.config.Rps)) || (atomic.AddUint32(&s.getCount, 1) > uint32(s.config.Rps)) {
		s.sendToPeer(w, req)

		return
	}*/

	s.muReqCount.Lock()

	if s.reqCount < uint32(s.config.Rps) && (atomic.AddUint32(&s.getCount, 1) <= uint32(s.config.Rps)) {
		s.reqCount++

		s.muReqCount.Unlock()

		s.response(&w, req)

		s.muReqCount.Lock()
		s.reqCount--
		s.muReqCount.Unlock()

		return
	}

	s.muReqCount.Unlock()

	s.sendToPeer(w, req)
}

func (s *Server) alivedChecker() {
	t := time.NewTicker(time.Second * ALIVED_CHECKER_TIMEOUT)

	checker := func() {
		for i, worker := range s.workers {
			if !worker.alived {
				timeout := time.Second * ALIVED_CONN_TIMEOUT
				conn, err := net.DialTimeout("tcp", worker.address, timeout)

				if err != nil {
					log.Println("Site unreachable, error: ", err.Error())

					continue
				}

				_ = conn.Close()

				s.workers[i].alived = true
			}
		}
	}

	for {
		select {
		case <-t.C:
			//log.Println("alived checker")

			checker()
		}
	}
}

func (s *Server) clearRequestCounter() {
	t := time.NewTicker(time.Second)

	for {
		select {
		case <-t.C:
			//log.Println("clear request counter")

			atomic.StoreUint32(&s.getCount, 0)

			s.muNextWorker.Lock()

			for i, _ := range s.workers {
				atomic.StoreUint32(&s.workers[i].getCount, 0)
			}

			s.muNextWorker.Unlock()
		}
	}
}

func (s *Server) Run() {
	var hf func(http.ResponseWriter, *http.Request)

	if s.config.IsMaster {
		hf = s.handleMaster

		go s.clearRequestCounter()
		go s.alivedChecker()

		for ind, _ := range s.workers {
			i := ind

			s.workers[i].proxy.ErrorHandler = func(w http.ResponseWriter, req *http.Request, err error) {
				var opErr *net.OpError
				if errors.As(err, &opErr) {
					s.workers[i].alived = false

					/*dump, _ := httputil.DumpRequest(req, true)
					log.Println(string(dump))*/

					s.handleMaster(w, req)
				} else {
					log.Printf("Proxy %d err(%s) [%+v]", i, err.Error(), reflect.TypeOf(err))

					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}

				atomic.AddUint32(&s.workers[i].reqCount, ^uint32(0))
			}

			s.workers[i].proxy.ModifyResponse = func(*http.Response) error {
				atomic.AddUint32(&s.workers[i].reqCount, ^uint32(0))

				return nil
			}
		}

	} else {
		hf = s.handleWorker
	}

	address := s.config.Host + ":" + strconv.Itoa(int(s.config.Port))

	server := &http.Server{
		Addr:    address,
		Handler: http.HandlerFunc(hf),
	}

	log.Printf("Start http server: %s rps:%d\n", address, s.config.Rps)
	err := server.ListenAndServe()

	if err != nil {
		panic(err.Error())
	}

	log.Println("Stop server")
}
