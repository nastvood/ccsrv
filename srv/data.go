package srv

type Order struct {
  Price uint `json: "price"`
  Quantity uint `json: "quantity"`
  Amount uint `json: "amount"`
  Object uint `json: "object"`
  Method uint `json: "method"`
}
