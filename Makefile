build_docker: build
	sudo docker-compose build

build:
	go build -o bin/ccsrv ccsrv

clean:
	rm -rf ./bin

start:
	sudo docker-compose --env-file .env.docker up -d

stop:
	sudo docker-compose down

restart: stop start
